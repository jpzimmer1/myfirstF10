package de.fiduciagad.prototyp.f10.dashboard.controllers;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	/*
	 * F10 MVP - Webservice, der unter /api/data erreichbar ist, der für unseren Protoyp Dummydaten generiert
	 */

	@RequestMapping("/api/data")
	public String data() {
		final ThreadLocalRandom random = ThreadLocalRandom.current();
		return String.format("[%d, %d, %d, %d, %d, %d,%d, %d, %d, %d, %d, %d, %d, %d, %d, %d]", random.nextInt(200),
				random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200),
				random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200),
				random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200), random.nextInt(200));
	}

}
