package de.fiduciagad.prototyp.f10.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/*
 *	F10 MVP - Mit dieser Klasse wird die Anwendung initialisert  
 */

@SpringBootApplication
public class App extends SpringBootServletInitializer {

  public static void main(String[] args) throws Exception {
    SpringApplication.run(App.class, args);
  }

}